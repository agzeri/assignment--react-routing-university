# React Routing

Learning about [React Router v4](https://reacttraining.com/react-router/web/guides/philosophy)

## Documentation

+ [API Docs](https://harvardapi.docs.stoplight.io/api-reference)

## Setup Instructions

```sh
# (1) Clone the project
$ git clone https://gitlab.com/agzeri/assignment--react-routing-university

# (2) Enter in the created directory, and go to `starter-code`
$ cd assignment--react-routing-university/starter-code

# (3) Install all dependencies
$ npm install

# (4) Run the server
$ npm start
```

## Deliverables

### Sprint 1 | Configure React Routing

Install and configure the library with React. You just need to make sure that your app now it’s rendering `/#/` in the URL.

![](router-1.png)

### Sprint 2 | Create Home Route

+ *API Endpoint Documentation*: [https://harvardapi.heroukuapp.com/api/v1/classes](https://harvardapi.docs.stoplight.io/api-reference/classes/classes/get-classes)

Create a Home component and display it in the `/` route.

You should fetch API to get all available courses and display them in the page.

![](router-2.png)

### Sprint 3 | Create “About” Route

Now, add one more component. And create the needed routes.

![](router-3.png)

### Sprint 4 | Create a 404 Not Found Route

For those whose are navigating to non-existing routes, we want to make sure to display a gently error page.

*HINT: [Create a Not Match Component](https://reacttraining.com/react-router/web/example/no-match)*

![](router-4.png)

### Sprint 5 | Create the needed dynamic routes for `Classes`

Create a dynamic route with a router parameter. The route should looks like this: **`/classes/{anyClass}`**

**API Docs**

+ API Endpoint Documentation: [https://harvardapi.heroukuapp.com/api/v1/classes/{classId}](https://harvardapi.docs.stoplight.io/api-reference/classes/classes/get-class)
+ API Endpoint Documentation: [https://harvardapi.heroukuapp.com/api/v1/classes/{classId}/students](https://harvardapi.docs.stoplight.io/api-reference/classes/students/get-students)

![](router-5.mov)

### Sprint 6 | Create the needed dynamic routes for `Students`

+ API Endpoint Documentation: [https://harvardapi.heroukuapp.com/api/v1/students/{studentId}](https://harvardapi.docs.stoplight.io/api-reference/classes/students/get-student)

Create a dynamic route with a router parameter. The route should looks like this: **`/students/{anyStudent}`**

![](router-6.mov)

### Sprint 7 | Highlight the page

Highlight the menu option that was selected. So, we can know which page we are visiting.

*HINT: `NavLink`*

![](router-7.png)

### Sprint 8 | Add a “Loading” image

For UX purposes, add a loading image to give feedback to the user when a page it’s taking more time.

*HINT: You can use this [spinner](https://steamuserimages-a.akamaihd.net/ugc/267223080226459171/985912CAD9866AE8997702BB87823D78E1E66110/)*

![](router-8.png)

### Sprint 9 | Refactor Components to *Stateless Component*

Refactor those components who are not playing with the State.

*HINT: Remove `class` components and use simple `functions`. Don’t forget about how to send `props`.*

### Sprint 10 | Refactor API calls

Use a single file to handle all requests to API and re-use it in your components.

### Sprint 11 | Add “Top Students” Route

+ Add a button to each Student, you can click it and send them to a list about “Top Students”.
+ Create a new view and a route at `/top-students`, and render all students from that list.

### Sprint 12 | Refactor “Home” Route

Create an `IndexRoute` Component that render a Route to “Home”.

### Sprint 13 | BONUS | Route Transitions

Add some transitions to the app.

```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```